import gi
gi.require_version("Gtk", "3.0")

import re
import os
import pdb
import json
import pandas as pd
from gi.repository import Gtk
import mysql.connector as connector
from pprint import pprint
from datetime import datetime
from loguru import logger as LOGGER
from requests import request
from json import loads as jsondecode
from json import dumps as jsonencode


# list of tuples for each software, containing the software name, initial release, and main programming languages used
header_list = ["Produk", "Harga", "Qty", "Subtotal"]
data_list = []


class tbl_produk():
    def __init__(self, db_conn):
        self.cr = db_conn.cursor()
        self.mydb = db_conn

    def create_produk(self, nama, harga=0, stock=0):
        query = "insert into produk (nama, harga, stok) values(%s, %s, %s)"
        params = [nama, harga, stock]
        self.cr.execute(query, tuple(params))
        self.mydb.commit()
        return self.cr.lastrowid

    def get_produk_id(self, nama):
        query = "select id from produk where nama = %s limit 1"
        params = [nama]
        self.cr.execute(query, tuple(params))
        res = self.cr.fetchall()
        if not res or not res[0]:
            return False
        return res[0][0]


class tbl_header_penjualan():
    def __init__(self, db_conn):
        self.cr = db_conn.cursor()
        self.mydb = db_conn

    def create_header_penjualan(self, id_pelanggan, tanggal):
        o_now = datetime.now()
        query = "insert into header_penjualan (id_pelanggan, tanggal) values(%s, %s)"
        params = [id_pelanggan, tanggal]
        self.cr.execute(query, tuple(params))
        res = self.cr.lastrowid
        nomor = "{}/{}/{:05d}".format(o_now.year, o_now.month, res)
        q_update = "update header_penjualan set nomor=%s where id=%s"
        p_update = [nomor, res]
        self.cr.execute(q_update, tuple(p_update))
        self.mydb.commit()
        return res


class ReportWindow(Gtk.Window):
    def __init__(self):
        # init main window format
        Gtk.Window.__init__(self, title="Report")
        self.set_border_width(10)
        hbox = Gtk.Box(spacing=6)
        hbox.set_homogeneous(False)
        box_main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box_main.set_homogeneous(False)

        box_form = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        box_form.set_homogeneous(False)

        vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_left.set_homogeneous(False)
        vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_right.set_homogeneous(False)

        # add wellcome message
        label = Gtk.Label(label="Download Report")
        box_main.pack_start(label, True, True, 0)

        box_form.pack_start(vbox_left, True, True, 0)
        box_form.pack_start(vbox_right, True, True, 0)
        box_main.pack_start(box_form, True, True, 0)
        hbox.pack_start(box_main, True, True, 0)

        # add button
        # button = Gtk.Button.new_with_label("Registrasi User")
        # button.connect("clicked", self.open_user_window)
        # box_main.pack_start(button, True, True, 0)

        # button = Gtk.Button.new_with_label("Login")
        # button.connect("clicked", self.open_login_window)
        # box_main.pack_start(button, True, True, 0)

        label = Gtk.Label("Mulai dari: ")
        vbox_left.pack_start(label, True, True, 0)
        self.tgl_mulai = Gtk.Entry()
        vbox_right.pack_start(self.tgl_mulai, True, True, 0)
    
        label = Gtk.Label("Sampai dengan: ")
        vbox_left.pack_start(label, True, True, 0)
        self.tgl_akhir = Gtk.Entry()
        vbox_right.pack_start(self.tgl_akhir, True, True, 0)

        button = Gtk.Button.new_with_label("Choose directory")
        button.connect("clicked", self.on_folder_clicked)
        vbox_left.pack_start(button, True, True, 0)
        self.labelframe = Gtk.Label(label="")
        vbox_right.pack_start(self.labelframe, True, True, 0)

        button = Gtk.Button.new_with_label("Generate Report")
        button.connect("clicked", self.generate_report)
        box_main.pack_start(button, True, True, 0)

        self.add(hbox)

    def generate_report(self, button):
        print("save to: {}".format(self.labelframe.get_text()))
        raw_data = {
            'start_date': self.tgl_mulai.get_text(),
            'end_date': self.tgl_akhir.get_text(),
        }
        data = jsonencode(raw_data)
        response = request(
            'POST',
            'http://localhost:9090/report/create',
            headers={'api-key': '31a2658b-e907-4826-9758-07be33374510'},
            data=data,
        )
        obj = helper()
        if response.status_code != 200:
            raw_data = response.text
            data = jsondecode(raw_data)
            err = data.get('msg')
            obj.report_error(str(err))
            return
        # save document here
        data = json.loads(response.text)["msg"]
        # bangun data untuk dataframe
        if not data:
            obj.report_error("No data available")
            return
        output_data = {}
        for row in data:
            for d in row:
                if d not in output_data:
                    output_data[d] = []
                output_data[d].append(row[d])
        output_file = "{}/output.xls".format(self.labelframe.get_text())
        LOGGER.info("Writing {}".format(output_file))
        excel_writer = pd.ExcelWriter(output_file, engine="xlsxwriter")
        df = pd.DataFrame(output_data)
        df.to_excel(excel_writer, index=False)
        workbook  = excel_writer.book
        worksheet = excel_writer.sheets['Sheet1']
        # Write the column headers with the defined format.
        header_format = workbook.add_format({
            'bold': True,
            'text_wrap': True,
            'valign': 'top',
            'fg_color': '#D7E4BC',
            'border': 1})
        try:
            excel_writer.save()
        except Exception as err:
            obj.report_error(str(err))
            return
        obj.report_success("File {} created successfully".format(output_file))
        return

    def on_folder_clicked(self, widget):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a folder",
            parent=self,
            action=Gtk.FileChooserAction.SELECT_FOLDER,
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, "Select", Gtk.ResponseType.OK
        )
        dialog.set_default_size(800, 400)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            file_path = dialog.get_filename()
            print("Save clicked")
            print("File selected: " + file_path)
            self.labelframe.set_label(os.path.abspath(file_path))
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()


class HomeWindow(Gtk.Window):
    def __init__(self):
        # init main window format
        Gtk.Window.__init__(self, title="Home")
        self.set_border_width(10)
        hbox = Gtk.Box(spacing=6)
        hbox.set_homogeneous(False)
        box_main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box_main.set_homogeneous(False)

        vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_left.set_homogeneous(False)
        vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_right.set_homogeneous(False)

        box_main.pack_start(vbox_left, True, True, 0)
        box_main.pack_start(vbox_right, True, True, 0)
        hbox.pack_start(box_main, True, True, 0)

        # add wellcome message
        label = Gtk.Label(label="Selamat datang di aplikasi penjualan")
        box_main.pack_start(label, True, True, 0)

        # add button
        # button = Gtk.Button.new_with_label("Registrasi User")
        # button.connect("clicked", self.open_user_window)
        # box_main.pack_start(button, True, True, 0)

        # button = Gtk.Button.new_with_label("Login")
        # button.connect("clicked", self.open_login_window)
        # box_main.pack_start(button, True, True, 0)

        button = Gtk.Button.new_with_label("Jual Barang")
        button.connect("clicked", self.open_sale_window)
        box_main.pack_start(button, True, True, 0)

        button = Gtk.Button.new_with_label("Tambah pelanggan")
        button.connect("clicked", self.open_customer_window)
        box_main.pack_start(button, True, True, 0)

        button = Gtk.Button.new_with_label("Tambah produk")
        button.connect("clicked", self.open_product_window)
        box_main.pack_start(button, True, True, 0)

        button = Gtk.Button.new_with_label("Laporan")
        button.connect("clicked", self.open_report_window)
        box_main.pack_start(button, True, True, 0)

        self.add(hbox)

    def open_report_window(self, button):
        window = ReportWindow()
        window.show_all()

    def open_sale_window(self, button):
        window = MyWindow()
        window.show_all()
        # window.connect("destroy", Gtk.quit)
    
    def open_product_window(self, button):
        window = ProductWindow()
        window.show_all()

    def open_customer_window(self, button):
        window = CustomerWindow()
        window.show_all()
        # window.connect("destroy", Gtk.quit)


class ProductWindow(Gtk.Window):
    def __init__(self):
        # buat koneksi ke database
        self.mydb = connector.connect(
            host="localhost",
            user="root",
            password="",
            database="jualan_db"
        )
        self.cr = self.mydb.cursor()

         # init main window format
        Gtk.Window.__init__(self, title="Tambah Produk")
        self.set_border_width(10)
        hbox = Gtk.Box(spacing=6)
        hbox.set_homogeneous(False)
        box_main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box_main.set_homogeneous(False)

        box_form = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        box_form.set_homogeneous(False)

        vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_left.set_homogeneous(False)
        vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_right.set_homogeneous(False)

        # add wellcome message
        label = Gtk.Label(label="Isi data produk")
        box_main.pack_start(label, True, True, 0)

        box_form.pack_start(vbox_left, True, True, 0)
        box_form.pack_start(vbox_right, True, True, 0)
        box_main.pack_start(box_form, True, True, 0)
        hbox.pack_start(box_main, True, True, 0)


        # buat form
        label = Gtk.Label(label="Nama Produk")
        vbox_left.pack_start(label, True, True, 0)
        self.name = Gtk.Entry()
        self.name.set_visibility(False)
        vbox_right.pack_start(self.name, True, True, 0)

        label = Gtk.Label(label="Harga")
        vbox_left.pack_start(label, True, True, 0)
        adjustment = Gtk.Adjustment(upper=1000000000, step_increment=100, page_increment=10)
        self.harga = Gtk.SpinButton()
        self.harga.set_adjustment(adjustment)
        vbox_right.pack_start(self.harga, True, True, 0)
        x = [y for y in list_obj]
        # add button simpan
        button = Gtk.Button.new_with_label("Simpan")
        button.connect("clicked", self.simpan_produk)
        box_main.pack_start(button, True, True, 0)

        self.add(hbox)
    
    def simpan_produk(self, button):
        query = """
        insert into produk (
            nama,
            harga
        )
        values (
            %s,
            %s
        )"""
        params = [
            self.name.get_text(),
            self.harga.get_text()
            ]
        self.cr.execute(query, tuple(params))
        print(query % tuple(params))
        self.mydb.commit()


class CustomerWindow(Gtk.Window):
    def __init__(self):
        # buat koneksi ke database
        self.mydb = connector.connect(
            host="localhost",
            user="root",
            password="",
            database="jualan_db"
        )
        self.cr = self.mydb.cursor()

         # init main window format
        Gtk.Window.__init__(self, title="Home")
        self.set_border_width(10)
        hbox = Gtk.Box(spacing=6)
        hbox.set_homogeneous(False)
        box_main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box_main.set_homogeneous(False)

        box_form = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        box_form.set_homogeneous(False)

        vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_left.set_homogeneous(False)
        vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_right.set_homogeneous(False)

        # add wellcome message
        label = Gtk.Label(label="Isi data pelanggan")
        box_main.pack_start(label, True, True, 0)

        box_form.pack_start(vbox_left, True, True, 0)
        box_form.pack_start(vbox_right, True, True, 0)
        box_main.pack_start(box_form, True, True, 0)
        hbox.pack_start(box_main, True, True, 0)


        # buat form
        label = Gtk.Label(label="Nama Lengkap")
        vbox_left.pack_start(label, True, True, 0)
        self.name = Gtk.Entry()
        vbox_right.pack_start(self.name, True, True, 0)

        label = Gtk.Label(label="No HP")
        vbox_left.pack_start(label, True, True, 0)
        self.hp = Gtk.Entry()
        vbox_right.pack_start(self.hp, True, True, 0)

        label = Gtk.Label(label="Alamat")
        vbox_left.pack_start(label, True, True, 0)
        self.alamat = Gtk.Entry()
        vbox_right.pack_start(self.alamat, True, True, 0)

        # add button simpan
        button = Gtk.Button.new_with_label("Simpan")
        button.connect("clicked", self.simpan_pelanggan)
        box_main.pack_start(button, True, True, 0)

        self.add(hbox)
    
    def simpan_pelanggan(self, button):
        x = re.search("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$", self.hp.get_text())
        obj = helper()
        if not x:
            obj.report_error("No HP tidak diisi dengan benar")
            return
        raw_data = {
            'nama': self.name.get_text(),
            'hp': self.hp.get_text(),
            'alamat': self.alamat.get_text()
        }
        data = jsonencode(raw_data)
        response = request(
            'POST',
            'http://localhost:8111/pelanggan/create',
            headers={'api-key': '31a2658b-e907-4826-9758-07be33374510'},
            data=data,
            params={'tes': 123}
        )
        if response.status_code != 200:
            raw_data = response.text
            data = jsondecode(raw_data)
            err = data.get('msg')
            obj.report_error(str(err))
            return
        obj.report_success("Simpan pelanggan {}".format(self.name.get_text()))
        return


class MyWindow(Gtk.Window):
    def __init__(self):
        # buat koneksi ke database
        self.mydb = connector.connect(
            host="localhost",
            user="root",
            password="",
            database="jualan_db"
        )
        self.cr = self.mydb.cursor()
        self.id_header = False  # set it later after user add first product

        # init main window format
        Gtk.Window.__init__(self, title="Penjualan")
        self.set_border_width(10)
        hbox = Gtk.Box(spacing=6)
        hbox.set_homogeneous(False)
        box_main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box_main.set_homogeneous(False)
        
        box_header = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        box_header.set_homogeneous(False)
        box_container = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        box_container.set_homogeneous(False)
        box_data = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        box_data.set_homogeneous(False)

        vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_left.set_homogeneous(False)
        vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox_right.set_homogeneous(False)
        
        hbox.pack_start(box_main, True, True, 0)
        box_main.pack_start(box_header, True, True, 0)
        box_main.pack_start(box_container, True, True, 0)
        box_main.pack_start(box_data, True, True, 0)
        box_container.pack_start(vbox_left, True, True, 0)
        box_container.pack_start(vbox_right, True, True, 0)

        # insert component into form
        # sequently from top to bottom
        label = Gtk.Label(label="Tanggal")
        box_header.pack_start(label, True, True, 0)
        date_sale = Gtk.Label(label=datetime.now().strftime("%Y-%m-%d"))
        box_header.pack_start(date_sale, True, True, 0)

        # get customer from db
        customer_list = Gtk.ListStore(int, str)
        response = request(
            'GET',
            'http://localhost:8111/pelanggan/list',
            headers={'api-key': '31a2658b-e907-4826-9758-07be33374510'}
        )
        data = jsondecode(response.text)
        if response.status_code != 200:
            obj = helper()
            obj.report_error("Connection to server error\n{}".format(data.get('msg')))
            return
        res = data['data']
        for row in res:
            pprint(list(row))
            customer_list.append(row)

        label = Gtk.Label(label="Nama customer")
        box_header.pack_start(label, True, True, 0)
        self.nama_customer = Gtk.ComboBox.new_with_model_and_entry(customer_list)
        self.nama_customer.set_entry_text_column(1)
        box_header.pack_start(self.nama_customer, True, True, 0)

        # get product from db
        product_list = Gtk.ListStore(int, str)
        query = "select id, nama from produk"
        self.cr.execute(query)
        res = self.cr.fetchall()
        for row in res:
            pprint(list(row))
            product_list.append(row)

        label = Gtk.Label(label="Produk")
        vbox_left.pack_start(label, True, True, 0)
        self.produk = Gtk.ComboBox.new_with_model_and_entry(product_list)
        self.produk.set_entry_text_column(1)
        vbox_right.pack_start(self.produk, True, True, 0)

        label = Gtk.Label(label="Harga")
        vbox_left.pack_start(label, True, True, 0)
        adjustment = Gtk.Adjustment(upper=1000000000, step_increment=100, page_increment=10)
        self.harga = Gtk.SpinButton()
        self.harga.set_adjustment(adjustment)
        vbox_right.pack_start(self.harga, True, True, 0)

        label = Gtk.Label(label="Qty")
        vbox_left.pack_start(label, True, True, 0)
        adjustment = Gtk.Adjustment(upper=100, step_increment=1, page_increment=10)
        self.qty = Gtk.SpinButton()
        self.qty.set_adjustment(adjustment)
        vbox_right.pack_start(self.qty, True, True, 0)

        label = Gtk.Label(label="Subtotal")
        vbox_left.pack_start(label, True, True, 0)
        self.subtotal = Gtk.Label(label="0")
        vbox_right.pack_start(self.subtotal, True, True, 0)

        button = Gtk.Button.new_with_label("Tambah")
        button.connect("clicked", self.tambah_produk)
        box_container.pack_start(button, True, True, 0)

        # Setting up the self.grid in which the elements are to be positionned
        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(True)
        self.grid.set_row_homogeneous(True)
        box_data.pack_start(self.grid, True, True, 0)

        # Creating the ListStore model
        self.data_liststore = Gtk.ListStore(str, str, str, str)
        for row in data_list:
            self.data_liststore.append(list(row))
        self.current_filter_language = None

        # Creating the filter, feeding it with the liststore model
        self.language_filter = self.data_liststore.filter_new()
        # setting the filter function, note that we're not using the
        self.language_filter.set_visible_func(self.language_filter_func)

        # creating the treeview, making it use the filter as a model, and adding the columns
        self.treeview = Gtk.TreeView.new_with_model(self.language_filter)
        for i, column_title in enumerate(
            header_list
        ):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i)
            self.treeview.append_column(column)

        # creating buttons to filter by programming language, and setting up their events
        self.buttons = list()
        button = Gtk.Button(label="Close")
        self.buttons.append(button)
        button.connect("clicked", self.on_close_clicked)
        # for prog_language in ["Java", "C", "C++", "Python", "None"]:
        #     button = Gtk.Button(label=prog_language)
        #     self.buttons.append(button)
        #     button.connect("clicked", self.on_selection_button_clicked)

        # setting up the layout, putting the treeview in a scrollwindow, and the buttons in a row
        self.scrollable_treelist = Gtk.ScrolledWindow()
        self.scrollable_treelist.set_vexpand(True)
        self.grid.attach(self.scrollable_treelist, 0, 0, 8, 10)
        # set button position
        self.grid.attach_next_to(
            self.buttons[0], self.scrollable_treelist, Gtk.PositionType.BOTTOM, 1, 1
        )
        # for i, button in enumerate(self.buttons[1:]):
        #     self.grid.attach_next_to(
        #         button, self.buttons[i], Gtk.PositionType.RIGHT, 1, 1
        #     )
        self.scrollable_treelist.add(self.treeview)

        self.add(hbox)
        # button = Gtk.Button.new_with_label("Click Me")
        # button.connect("clicked", self.on_click_me_clicked)
        # hbox.pack_start(button, True, True, 0)

        # button = Gtk.Button.new_with_mnemonic("_Open")
        # button.connect("clicked", self.on_open_clicked)
        # hbox.pack_start(button, True, True, 0)

        # button = Gtk.Button.new_with_mnemonic("_Close")
        # button.connect("clicked", self.on_close_clicked)
        # hbox.pack_start(button, True, True, 0)

    def get_customer_id(self):
        # get combo box text
        tree_iter = self.nama_customer.get_active_iter()
        if tree_iter is not None:
            model = self.nama_customer.get_model()
            customer_id, customer_name = model[tree_iter]
            return customer_id
        return False

    def tambah_produk(self, button):
        # cek customer field
        cus_id = self.get_customer_id()
        if not cus_id:
            self.report_error("customer is not filled yet")
            return
        obj_header = tbl_header_penjualan(self.mydb)
        if not self.id_header:
            try:
                self.id_header = obj_header.create_header_penjualan(cus_id,
                                                                    datetime.now()
                                                                    .strftime("%Y-%m-%d"))
            except Exception as err:
                self.report_error(str(err))
                return
        qty = int(self.qty.get_text() or 0)
        harga = int(self.harga.get_text() or 0)
        subtotal = qty * harga

        # get combo box text
        tree_iter = self.produk.get_active_iter()
        if tree_iter is not None:
            model = self.produk.get_model()
            produk_id, produk_name = model[tree_iter]
        else:
            new_produk = self.produk.get_child().get_text()
            if not new_produk:
                LOGGER.info("No product entered")
                return
            obj_produk = tbl_produk(self.mydb)
            try:
                produk_id = obj_produk.get_produk_id(new_produk)
                if not produk_id:
                    produk_id = obj_produk.create_produk(new_produk)
            except Exception as err:
                self.report_error(str(err))
                return
            produk_name = new_produk
        self.subtotal.set_text(str(subtotal))
        row = (
            str(produk_name),
            str(harga),
            str(qty),
            str(subtotal)
        )
        self.data_liststore.append(list(row))
        # method untuk nyimpen data ke database
        # query insert .....
        query = """
        insert into penjualan (
            id_header,
            id_produk,
            harga,
            qty,
            subtotal
        )
        values (
            %s,
            %s,
            %s,
            %s,
            %s
        )"""
        params = [
            self.id_header,
            produk_id,
            harga,
            qty,
            subtotal
            ]
        try:
            self.cr.execute(query, tuple(params))
            print(query % tuple(params))
            self.mydb.commit()
        except Exception as err:
            self.report_error(str(err))
        return

    def on_click_me_clicked(self, button):
        print('"Click me" button was clicked')
        txt = self.nama_customer.get_text()
        print('TXT: %s' % txt)

    def on_open_clicked(self, button):
        print('"Open" button was clicked')

    def on_close_clicked(self, button):
        print("Closing application")
        Gtk.main_quit()

    def btn_pressed(self, widget):
        print("Button clicked....")

    def language_filter_func(self, model, iter, data):
        """Tests if the language in the row is the one in the filter"""
        if (
            self.current_filter_language is None
            or self.current_filter_language == "None"
        ):
            return True
        else:
            return model[iter][2] == self.current_filter_language

    def on_selection_button_clicked(self, widget):
        """Called on any of the button clicks"""
        # we set the current language filter to the button's label
        self.current_filter_language = widget.get_label()
        print("%s language selected!" % self.current_filter_language)
        # we update the filter, which updates in turn the view
        self.language_filter.refilter()

    def report_error(self, reason):
        dialog = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Something went wrong")
        dialog.format_secondary_text(reason)
        dialog.run()
        dialog.destroy()


class helper():
    def report_error(self, reason):
        dialog = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Something went wrong")
        dialog.format_secondary_text(reason)
        dialog.run()
        dialog.destroy()

    def report_success(self, what):
        dialog = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Congrats")
        dialog.format_secondary_text(what)
        dialog.run()
        dialog.destroy()

if __name__ == '__main__':
    window = HomeWindow()
    window.show_all()
    window.connect("destroy", Gtk.main_quit)
    Gtk.main()
